import { Assumption } from '@/shared/models/assumption.model';

export class AssumptionWithAggResponses {
  constructor(
    public assumption: Assumption,
    public posResponsesRate: number,
    public responsesCount: number) { }
}
