# TODO list

Last updated on 29.03.2019

## Top priority

1. Complete switching from mock services to actual API to backend. (#2)
2. Add login form (with login and passsword) and authenticate through /login to receive auth-token. (#3)
3. Make sure to pass auth-token to requests towards API when user is authenticated. (#4)
4. Make sure to support alternative authentication through UUID (to be used for generated email links). (#5)
5. Improve AuthGuard to redirect unauthorized users to login form and back to originally requested page. (#6)
6. Add /users/{userId}/changePassword operation to: API, server and UI. It should send password in PUT and crypt it in backend. (#7)

## Features

* Create / manage survey (#8)
* More reports
* Create / manage users (by admin) (#9)

## Security

* Use HTTPS for backend (should work but needs certificate and testing) (#10)
* Use better token authentication for backend services (actual tokens instead of UUID) (#12)
* Add authorization to backend services so that user can only see / change their stuff (except admin) (#13)
* Add better authentication (OAuth2?) (#14)
* Active directory / domain authentication (#15)
* Add role based authorization (group admin / survey creator etc) (#16)

## Testing

* Write more backend unit tests (#17)
* Write more backend end-to-end tests (#18)
* Write more ui tests (#19)
* Connect to GitLab CI (#20)

## Other

* Better error handling with better messages in backend services (#21)
* Improve logging in backend services (#22)
* Add some backend services which actually calculate reports in backend (instead of being mostly CRUD)
