import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-assumption-response',
  templateUrl: './assumption-response.component.html',
  styleUrls: ['../questionnaire-responses.component.css',
    './assumption-response.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AssumptionWithResponseComponent),
      multi: true
    }
  ]
})
export class AssumptionWithResponseComponent implements ControlValueAccessor {
  @Input() assumption_text: string;

  positiveSelected = false;
  negativeSelected = false;

  // Function to call when the response is given.
  onChange = (response: boolean) => { };
  // Function to call when the input is touched.
  onTouched = () => { };

  get value(): boolean {
    if (this.positiveSelected || this.negativeSelected) {
      return this.positiveSelected;
    }
    return null;
  }

  writeValue(response: boolean): void {
    if (response != null) {
      this.positiveSelected = response;
      this.negativeSelected = !response;
    }
    this.onChange(response);
  }

  registerOnChange(fn: (response: boolean) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
}
