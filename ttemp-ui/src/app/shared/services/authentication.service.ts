import { Injectable } from '@angular/core';

import { AppUser } from '@/shared/models/app-user.model';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

const TOKEN_PROPERTY_NAME = 'user_token';
const USER_PROPERTY_NAME = 'current_user';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    constructor(private apiService: ApiService) { }

    public set token(token: string) {
        localStorage.setItem(TOKEN_PROPERTY_NAME, token);
    }

    public get token(): string {
        return localStorage.getItem(TOKEN_PROPERTY_NAME);
    }

    public set appUser(appUser: AppUser) {
        localStorage.setItem(USER_PROPERTY_NAME, JSON.stringify(appUser));
    }

    public get appUser(): AppUser {
        return JSON.parse(localStorage.getItem(USER_PROPERTY_NAME));
    }

    public clearToken() {
        localStorage.removeItem(TOKEN_PROPERTY_NAME);
        localStorage.removeItem(USER_PROPERTY_NAME);
    }

    public get isAuthenticated() {
        return this.token !== null;
    }

    authenticate(userToken: string): Promise<boolean> {
        return this.apiService.usersGet(userToken).pipe(
            map(appUser => {
                if (appUser !== null) {
                    this.token = userToken;
                    this.appUser = appUser;
                } else {
                    this.clearToken();
                }
                return this.isAuthenticated;
            })
        ).toPromise();
    }
}
