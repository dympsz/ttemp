package crypt

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGeneratesHashFromPassword(t *testing.T) {
	assert := assert.New(t)

	password := "abc123"

	hash, err := PasswordToHash(password)

	assert.Nil(err, "Should not return error")
	assert.NotEmpty(hash, "Hash should not be empty")

	hashStr := string(hash[:])
	t.Logf("Hash: %s", hashStr)
}

func TestPasswordCorrect(t *testing.T) {
	assert := assert.New(t)

	password := "abc123"
	hash := []byte("16384$8$1$f7f872728154b60880aecce552ac6b15$bd8dc1a6e2855bd41a36615cc66608c088114e8cdd95714882b13d4a0ee50b4a")

	err := CheckPasswordWithHash(password, hash)

	assert.Nil(err, "Password should match hash")
}

func TestPasswordIncorrect(t *testing.T) {
	assert := assert.New(t)

	password := "abc123"
	hash := []byte("16384$8$1$f7f872728154b60881aecce552ac6b15$bd8dc1a6e2855bd41a36615cc66608c088114e8cdd95714882b13d4a0ee11111")

	err := CheckPasswordWithHash(password, hash)

	assert.NotNil(err, "Password should not match hash")
}

func TestHashPasswordAndCheck(t *testing.T) {
	assert := assert.New(t)

	password := "abc123"

	hash, err := PasswordToHash(password)
	assert.Nil(err, "Should not return error")
	assert.NotEmpty(hash, "Hash should not be empty")

	hashStr := string(hash[:])
	t.Logf("Hash: %s", hashStr)
	newHash := []byte(hashStr)

	err = CheckPasswordWithHash(password, newHash)
	assert.Nil(err, "Password should match hash")
}
