import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';

import { AssumptionWithAggResponses } from './assumption-agg-responses/assumption-agg-responses.model';
import { Assumption } from '@/shared/models/assumption.model';

@Injectable({
  providedIn: 'root'
})
export class StudyWithAggResponsesService {
  public getStudyWithAggResponses(): Observable<AssumptionWithAggResponses[]> {
    const study: AssumptionWithAggResponses[] = [];

    function addResults(assumptionId: number, assumption_text: string, posResponsesRate, responsesCount) {
      study.push(new AssumptionWithAggResponses(new Assumption(assumptionId, assumption_text), posResponsesRate, responsesCount));
    }
    addResults(1, 'Birds flyin\' high, you know how I feel', 0.6, 5);
    addResults(2, 'Sun in the sky, you know how I feel', 1.0, 5);
    addResults(3, 'Breeze driftin\' on by, you know how I feel', 0.8, 5);
    addResults(4, 'It\'s a new dawn, it\'s a new day, it\'s a new life for me', 0.2, 5);
    addResults(5, 'Yeah, it\'s a new dawn, it\'s a new day, it\'s a new life for me', 1.0, 5);
    addResults(6, 'And I\'m feelin\' good', 0.0, 1);

    return of(study);
  }
}
