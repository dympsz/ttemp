// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"

	models "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

// DeleteAssumptionsAssumptionIDHandlerFunc turns a function with the right signature into a delete assumptions assumption ID handler
type DeleteAssumptionsAssumptionIDHandlerFunc func(DeleteAssumptionsAssumptionIDParams, *models.AppUser) middleware.Responder

// Handle executing the request and returning a response
func (fn DeleteAssumptionsAssumptionIDHandlerFunc) Handle(params DeleteAssumptionsAssumptionIDParams, principal *models.AppUser) middleware.Responder {
	return fn(params, principal)
}

// DeleteAssumptionsAssumptionIDHandler interface for that can handle valid delete assumptions assumption ID params
type DeleteAssumptionsAssumptionIDHandler interface {
	Handle(DeleteAssumptionsAssumptionIDParams, *models.AppUser) middleware.Responder
}

// NewDeleteAssumptionsAssumptionID creates a new http.Handler for the delete assumptions assumption ID operation
func NewDeleteAssumptionsAssumptionID(ctx *middleware.Context, handler DeleteAssumptionsAssumptionIDHandler) *DeleteAssumptionsAssumptionID {
	return &DeleteAssumptionsAssumptionID{Context: ctx, Handler: handler}
}

/*DeleteAssumptionsAssumptionID swagger:route DELETE /assumptions/{assumptionId} deleteAssumptionsAssumptionId

Delete an assumption

*/
type DeleteAssumptionsAssumptionID struct {
	Context *middleware.Context
	Handler DeleteAssumptionsAssumptionIDHandler
}

func (o *DeleteAssumptionsAssumptionID) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewDeleteAssumptionsAssumptionIDParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal *models.AppUser
	if uprinc != nil {
		principal = uprinc.(*models.AppUser) // this is really a models.AppUser, I promise
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
