import { async } from '@angular/core/testing';
import { Router } from '@angular/router';

import { AuthGuard } from './auth.guard';
import { AuthenticationService } from '../services/authentication.service';

describe('AuthGuard', () => {
    let guard: AuthGuard;
    let routerSpy: jasmine.SpyObj<Router>;
    let authServiceSpy: jasmine.SpyObj<AuthenticationService>;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('RouterMock', ['navigate']);
        authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['authenticate']);

        guard = new AuthGuard(<any>authServiceSpy, <any>routerSpy);
    }));

    it('should create a spy for router and auth service', () => {
        expect(routerSpy).toBeDefined();
        expect(authServiceSpy).toBeDefined();
    });

    it('should create', () => {
        expect(guard).toBeTruthy();
    });

    describe('canActivate', () => {
        it('should create', () => {
            expect(guard.canActivate).toBeDefined();
        });
    });
});
