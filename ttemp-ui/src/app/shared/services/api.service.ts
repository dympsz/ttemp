import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Survey } from '@/shared/models/survey.model';
import { Assumption } from '@/shared/models/assumption.model';
import { Response } from '@/shared/models/response.model';
import { AppUser } from '@/shared/models/app-user.model';

const SURVEY_LIST: Survey[] = [
    new Survey(1, 'Employee wellbeing', new Date('2019-02-01')),
    new Survey(2, 'Favourite sandwiches', new Date('2019-01-13'))
];

const ASSUMPTIONS: Assumption[] = [
    new Assumption(1, 'Birds flyin\' high, you know how I feel'),
    new Assumption(2, 'Sun in the sky, you know how I feel'),
    new Assumption(3, 'Breeze driftin\' on by, you know how I feel'),
    new Assumption(4, 'It\'s a new dawn, it\'s a new day, it\'s a new life for me'),
    new Assumption(5, 'Yeah, it\'s a new dawn, it\'s a new day, it\'s a new life for me'),
    new Assumption(6, 'And I\'m feelin\' good'),
];

const MOCK_TOKEN = 'correct_token';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    /**
     * List users
     *
     * @param uuid Filter by user UUID
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public usersGet(uuid?: string, observe?: 'body', reportProgress?: boolean): Observable<AppUser>;
    public usersGet(uuid?: string, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        return of(uuid === MOCK_TOKEN ? new AppUser(1, 'testuser', 'example@example.com', uuid) : null);
    }

    /**
    * List surveys
    *
    * @param userId Filter by user ID
    * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
    * @param reportProgress flag to report request and response progress.
    */
    public surveysGet(userId?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<Survey>>;
    public surveysGet(userId?: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        return of(SURVEY_LIST);
    }

    /**
     * List assumptions in survey
     *
     * @param surveyId
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public surveysSurveyIdAssumptionsGet(surveyId: number, observe?: 'body', reportProgress?: boolean): Observable<Array<Assumption>>;
    public surveysSurveyIdAssumptionsGet(surveyId: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        return of(ASSUMPTIONS);
    }

    /**
     * Get latest response
     *
     * @param userId
     * @param assumptionId
     * @param beforeDatetime Limit results to before datetime.
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public usersUserIdSurveysSurveyIdAssumptionsAssumptionIdResponsesLatestGet(userId: number, assumptionId: number,
        beforeDatetime?: Date, observe?: 'body', reportProgress?: boolean): Observable<Response>;
    public usersUserIdSurveysSurveyIdAssumptionsAssumptionIdResponsesLatestGet(userId: number, assumptionId: number,
        beforeDatetime?: Date, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        return of(new Response(1, 1, 1, 1, null, new Date('2019-03-06T21:34:00')));
    }
}
