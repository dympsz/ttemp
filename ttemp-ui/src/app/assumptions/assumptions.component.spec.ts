import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AssumptionsComponent } from './assumptions.component';
import { Assumption } from '@/shared/models/assumption.model';
import { DefaultService } from 'ttemp-api';

const ASSUMPTION_LIST: Assumption[] = [
  <Assumption> {id: 1, assumption_text: 'Assumption text 1'},
  <Assumption> {id: 2, assumption_text: 'Assumption text 2'}
];

describe('AssumptionsComponent', () => {
  let component: AssumptionsComponent;
  let fixture: ComponentFixture<AssumptionsComponent>;
  let defaultServiceSpy: jasmine.SpyObj<DefaultService>;

  beforeEach(async(() => {
    defaultServiceSpy = jasmine.createSpyObj('DefaultService', ['getAssumptions']);
    defaultServiceSpy.assumptionsGet.and.returnValue(of(ASSUMPTION_LIST));

    TestBed.configureTestingModule({
      declarations: [AssumptionsComponent],
      providers: [
        { provide: DefaultService, useValue: defaultServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssumptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a spy for DefaultService', () => {
    expect(defaultServiceSpy.assumptionsGet).toBeDefined();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get assumptions from service', () => {
    expect(component.assumptions).toBe(ASSUMPTION_LIST);
    expect(defaultServiceSpy.assumptionsGet).toHaveBeenCalledTimes(1);
  });
});
