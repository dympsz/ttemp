{
  "openapi": "3.0.2",
  "info": {
    "title": "Ttemp API",
    "description": "Team Temperature API",
    "contact": {
      "name": "Dymitr Pszenicyn",
      "url": "https://gitlab.com/dympsz",
      "email": "dpszenicyn@gmail.com"
    },
    "version": "1.0.5"
  },
  "paths": {
    "/assumptions": {
      "get": {
        "summary": "List assumptions",
        "parameters": [
          {
            "name": "surveyId",
            "in": "query",
            "description": "Filter by survey ID",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "List of assumptions.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Assumption"
                  }
                }
              }
            }
          },
          "404": {
            "description": "Assumption not found."
          }
        }
      },
      "post": {
        "summary": "Create assumption",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Assumption"
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "Assumption created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Assumption"
                }
              }
            }
          }
        }
      }
    },
    "/assumptions/{assumptionId}": {
      "get": {
        "summary": "Get an assumption",
        "responses": {
          "200": {
            "description": "Assumption found",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Assumption"
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Update an assumption",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Assumption"
              }
            }
          },
          "required": true
        },
        "responses": {
          "204": {
            "description": "Assumption updated"
          }
        }
      },
      "delete": {
        "summary": "Delete an assumption",
        "responses": {
          "204": {
            "description": "Assumption deleted"
          }
        }
      },
      "parameters": [
        {
          "name": "assumptionId",
          "in": "path",
          "description": "Assumption ID",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/users": {
      "get": {
        "summary": "List users",
        "parameters": [
          {
            "name": "uuid",
            "in": "query",
            "description": "Filter by user UUID",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "List of users",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/AppUser"
                  }
                }
              }
            }
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "post": {
        "summary": "Create user",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/AppUser"
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "User created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AppUser"
                }
              }
            }
          }
        }
      }
    },
    "/users/{userId}": {
      "get": {
        "summary": "Get user",
        "responses": {
          "200": {
            "description": "Found user",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AppUser"
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Update user",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/AppUser"
              }
            }
          },
          "required": true
        },
        "responses": {
          "204": {
            "description": "User updated"
          }
        }
      },
      "delete": {
        "summary": "Delete user",
        "responses": {
          "204": {
            "description": "User deleted"
          }
        }
      },
      "parameters": [
        {
          "name": "userId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/surveys": {
      "get": {
        "summary": "List surveys",
        "parameters": [
          {
            "name": "userId",
            "in": "query",
            "description": "Filter by user ID",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "List of surveys",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Survey"
                  }
                }
              }
            }
          },
          "404": {
            "description": "Survey not found"
          }
        }
      },
      "post": {
        "summary": "Create survey",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Survey"
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "Survey created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Survey"
                }
              }
            }
          }
        }
      }
    },
    "/surveys/{surveyId}": {
      "get": {
        "summary": "Get survey",
        "responses": {
          "200": {
            "description": "Get survey",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Survey"
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Update survey",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Survey"
              }
            }
          },
          "required": true
        },
        "responses": {
          "204": {
            "description": "Survey updated"
          }
        }
      },
      "delete": {
        "summary": "Delete survey",
        "responses": {
          "204": {
            "description": "Survey deleted"
          }
        }
      },
      "parameters": [
        {
          "name": "surveyId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/responses": {
      "get": {
        "summary": "List responses",
        "parameters": [
          {
            "name": "userId",
            "in": "query",
            "description": "Filter by user ID",
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "surveyId",
            "in": "query",
            "description": "Filter by survey ID",
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "assumptionId",
            "in": "query",
            "description": "Filter by assumption ID",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "List of responses",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Response"
                  }
                }
              }
            }
          }
        }
      },
      "post": {
        "summary": "Create response",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Response"
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "Response created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Response"
                }
              }
            }
          }
        }
      }
    },
    "/responses/{responseId}": {
      "get": {
        "summary": "Get response",
        "responses": {
          "200": {
            "description": "Response found",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Response"
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Update response",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Response"
              }
            }
          },
          "required": true
        },
        "responses": {
          "204": {
            "description": "Response updated"
          }
        }
      },
      "delete": {
        "summary": "Delete response",
        "responses": {
          "204": {
            "description": "Response deleted"
          }
        }
      },
      "parameters": [
        {
          "name": "responseId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/users/{userId}/surveys/{surveyId}/assumptions/{assumptionId}/responses": {
      "get": {
        "summary": "List responses",
        "responses": {
          "200": {
            "description": "List of responses",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Response"
                  }
                }
              }
            }
          }
        }
      },
      "parameters": [
        {
          "name": "userId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        },
        {
          "name": "surveyId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        },
        {
          "name": "assumptionId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/users/{userId}/surveys/{surveyId}/assumptions/{assumptionId}/responses/latest": {
      "get": {
        "summary": "Get latest response",
        "parameters": [
          {
            "name": "before_datetime",
            "in": "query",
            "description": "Limit results to before datetime.",
            "schema": {
              "format": "date-time",
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Latest response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Response"
                }
              }
            }
          },
          "404": {
            "description": "Not found"
          }
        }
      },
      "parameters": [
        {
          "name": "userId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        },
        {
          "name": "surveyId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        },
        {
          "name": "assumptionId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/surveys/{surveyId}/assumptions": {
      "get": {
        "summary": "List assumptions in survey",
        "responses": {
          "200": {
            "description": "List of assumptions in survey",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Assumption"
                  }
                }
              }
            }
          }
        }
      },
      "parameters": [
        {
          "name": "surveyId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/surveys/{surveyId}/assumptions/{assumptionId}/responses": {
      "get": {
        "summary": "List of responses for assumption in a survey",
        "responses": {
          "200": {
            "description": "List of responses for assumption in a survey",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Response"
                  }
                }
              }
            }
          }
        }
      },
      "parameters": [
        {
          "name": "assumptionId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        },
        {
          "name": "surveyId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    },
    "/surveys/{surveyId}/assumptions/{assumptionId}/responses/latestPerUser": {
      "get": {
        "summary": "Latest response for assumption per user in a survey",
        "parameters": [
          {
            "name": "before_datetime",
            "in": "query",
            "description": "Limit results to before datetime",
            "schema": {
              "format": "date-time",
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Latest response for assumption per user in a survey",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Response"
                  }
                }
              }
            }
          }
        }
      },
      "parameters": [
        {
          "name": "surveyId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        },
        {
          "name": "assumptionId",
          "in": "path",
          "required": true,
          "schema": {
            "type": "integer"
          }
        }
      ]
    }
  },
  "components": {
    "schemas": {
      "Assumption": {
        "title": "Root Type for Assumption",
        "description": "Assumption",
        "required": [],
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "assumption_text": {
            "type": "string"
          },
          "created_datetime": {
            "format": "date-time",
            "type": "string"
          }
        },
        "example": "{\n    \"id\": \"123\",\n    \"assumption_text\": \"Abc abc\",\n    \"created_datetime\": \"2019-01-01 17:23:45\"\n}"
      },
      "AppUser": {
        "title": "Root Type for AppUser",
        "description": "Application user",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "login": {
            "type": "string"
          },
          "email": {
            "type": "string"
          },
          "full_name": {
            "type": "string"
          },
          "user_uuid": {
            "type": "string"
          }
        },
        "example": "{\n    \"id\" : \"12\",\n    \"login\" : \"user1\",\n    \"email\" : \"user1@example.com\",\n    \"full_name\" : \"User One\",\n    \"user_uuid\" : \"c82506e8-3def-4329-8d96-c461904eb35c\"\n}"
      },
      "Survey": {
        "title": "Root Type for Survey",
        "description": "Survey",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "name": {
            "type": "string"
          },
          "start_datetime": {
            "format": "date-time",
            "type": "string"
          },
          "end_datetime": {
            "format": "date-time",
            "type": "string"
          }
        },
        "example": "{\n    \"id\" : \"123\",\n    \"name\" : \"Survey 1\",\n    \"start_datetime\" : \"2019-01-01 15:34:22\",\n    \"end_datetime\" : \"2019-06-01 19:56:44\"\n}"
      },
      "Response": {
        "title": "Root Type for Response",
        "description": "User response for an assumption in a survey.",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "survey_id": {
            "type": "integer"
          },
          "assumption_id": {
            "type": "integer"
          },
          "app_user_id": {
            "type": "integer"
          },
          "value": {
            "type": "boolean"
          },
          "response_datetime": {
            "format": "date-time",
            "type": "string"
          }
        },
        "example": "{\n    \"id\" : \"123\",\n    \"survey_id\" : \"111\",\n    \"assumption_id\" : \"222\",\n    \"app_user_id\" : \"333\",\n    \"value\" : \"false\",\n    \"response_datetime\" : \"2019-02-01 12:23:55\"\n}"
      }
    }
  }
}