/**
 * Ttemp API
 * Team Temperature API
 *
 * OpenAPI spec version: 1.2.4
 * Contact: dpszenicyn@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 * Application user
 */
export interface AppUser { 
    email?: string | null;
    full_name?: string | null;
    id?: number;
    is_admin?: boolean;
    login: string;
    password_hash?: string | null;
    user_uuid?: string;
}

