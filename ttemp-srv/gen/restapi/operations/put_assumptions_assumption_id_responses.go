// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

// PutAssumptionsAssumptionIDNoContentCode is the HTTP code returned for type PutAssumptionsAssumptionIDNoContent
const PutAssumptionsAssumptionIDNoContentCode int = 204

/*PutAssumptionsAssumptionIDNoContent Assumption updated

swagger:response putAssumptionsAssumptionIdNoContent
*/
type PutAssumptionsAssumptionIDNoContent struct {
}

// NewPutAssumptionsAssumptionIDNoContent creates PutAssumptionsAssumptionIDNoContent with default headers values
func NewPutAssumptionsAssumptionIDNoContent() *PutAssumptionsAssumptionIDNoContent {

	return &PutAssumptionsAssumptionIDNoContent{}
}

// WriteResponse to the client
func (o *PutAssumptionsAssumptionIDNoContent) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(204)
}

/*PutAssumptionsAssumptionIDDefault Error

swagger:response putAssumptionsAssumptionIdDefault
*/
type PutAssumptionsAssumptionIDDefault struct {
	_statusCode int
	/*The request id this is a response to

	 */
	XRequestID string `json:"X-Request-Id"`

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewPutAssumptionsAssumptionIDDefault creates PutAssumptionsAssumptionIDDefault with default headers values
func NewPutAssumptionsAssumptionIDDefault(code int) *PutAssumptionsAssumptionIDDefault {
	if code <= 0 {
		code = 500
	}

	return &PutAssumptionsAssumptionIDDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the put assumptions assumption ID default response
func (o *PutAssumptionsAssumptionIDDefault) WithStatusCode(code int) *PutAssumptionsAssumptionIDDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the put assumptions assumption ID default response
func (o *PutAssumptionsAssumptionIDDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithXRequestID adds the xRequestId to the put assumptions assumption ID default response
func (o *PutAssumptionsAssumptionIDDefault) WithXRequestID(xRequestID string) *PutAssumptionsAssumptionIDDefault {
	o.XRequestID = xRequestID
	return o
}

// SetXRequestID sets the xRequestId to the put assumptions assumption ID default response
func (o *PutAssumptionsAssumptionIDDefault) SetXRequestID(xRequestID string) {
	o.XRequestID = xRequestID
}

// WithPayload adds the payload to the put assumptions assumption ID default response
func (o *PutAssumptionsAssumptionIDDefault) WithPayload(payload *models.Error) *PutAssumptionsAssumptionIDDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the put assumptions assumption ID default response
func (o *PutAssumptionsAssumptionIDDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PutAssumptionsAssumptionIDDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	// response header X-Request-Id

	xRequestID := o.XRequestID
	if xRequestID != "" {
		rw.Header().Set("X-Request-Id", xRequestID)
	}

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
