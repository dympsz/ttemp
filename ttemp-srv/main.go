package main

//go:generate sqlboiler --wipe psql

import (
	"fmt"
	"os"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	_ "github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"

	loads "github.com/go-openapi/loads"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
	"gitlab.com/dympsz/ttemp/ttemp-srv/handlers"
)

// Command line arguments
// var portFlag = flag.Int("port", 8081, "Port to run this service on")

/*
	Generated go-swagger server already supports following command line arguments:
	--host=            the IP to listen on (default: localhost) [$HOST]
	--port=            the port to listen on for insecure connections, defaults to a random value [$PORT]
	--tls-host=        the IP to listen on for tls, when not specified it's the same as --host [$TLS_HOST]
	--tls-port=        the port to listen on for secure connections, defaults to a random value [$TLS_PORT]
	--tls-certificate= the certificate to use for secure connections [$TLS_CERTIFICATE]
	--tls-key=         the private key to use for secure conections [$TLS_PRIVATE_KEY]
*/

func main() {
	log.Init()

	// Parse command line arguments
	// flag.Parse()

	// Read environment
	var debugEnv = getEnv("DEBUG", "true")
	var servicePortEnv = getEnv("PORT", "8081")
	var postgresHostEnv = getEnv("POSTGRES_HOST", "localhost") // "192.168.99.100" on Docker Toolbox Windows
	var postgresDbEnv = getEnv("POSTGRES_DB", "postgres")
	var postgresUserEnv = getEnv("POSTGRES_USER", "postgres")
	var postgresPassEnv = getEnv("POSTGRES_PASSWORD", "ttemp-pass")

	var debug, err = strconv.ParseBool(debugEnv)
	if err != nil {
		log.Panic("Wrong debug", err)
	}
	servicePort, err := strconv.Atoi(servicePortEnv)
	if err != nil {
		log.Panic("Wrong service port number", err)
	}

	// Enable debug if needed
	boil.DebugMode = debug
	if debug {
		log.SetLevel(logrus.DebugLevel)
	}

	// Initialize DB
	var dbConnectionString = fmt.Sprintf("dbname=%s host=%s user=%s password=%s sslmode=disable",
		postgresDbEnv, postgresHostEnv, postgresUserEnv, postgresPassEnv)
	_, err = datastore.NewDB(dbConnectionString)
	if err != nil {
		log.Panic("Error connecting to DB", err)
	}
	defer datastore.CloseDB()

	// load embedded swagger file
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Panic("Error reading API definition", err)
	}

	// create new service API
	api := operations.NewTtempAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer server.Shutdown()

	// This can be removed if --port
	// set the port this service will be run on
	// server.Port = *portFlag
	server.Port = servicePort

	// TODO: remove this when `--tls-certificate` and `--tls-key` are configured correctly
	// This is a workaround to disable HTTPS configuration for now.
	server.EnabledListeners = []string{"http"}

	// set handlers
	handlers.InitAuth(api)
	handlers.InitLoginHandlers(api)
	handlers.InitAssumptionHandlers(api)
	handlers.InitResponseHandlers(api)
	handlers.InitSurveyHandlers(api)
	handlers.InitUserHandlers(api)

	// configure CORS etc
	server.ConfigureAPI()

	// serve API
	if err := server.Serve(); err != nil {
		log.Panic("Error serving API", err)
	}
}

func getEnv(key string, defaultValue string) string {
	value, ok := os.LookupEnv(key)
	if !ok {
		value = defaultValue
	}
	return value
}
