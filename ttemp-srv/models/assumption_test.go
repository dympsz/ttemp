// Code generated by SQLBoiler (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/randomize"
	"github.com/volatiletech/sqlboiler/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testAssumptions(t *testing.T) {
	t.Parallel()

	query := Assumptions()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testAssumptionsDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testAssumptionsQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := Assumptions().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testAssumptionsSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := AssumptionSlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testAssumptionsExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := AssumptionExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if Assumption exists: %s", err)
	}
	if !e {
		t.Errorf("Expected AssumptionExists to return true, but got false.")
	}
}

func testAssumptionsFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	assumptionFound, err := FindAssumption(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if assumptionFound == nil {
		t.Error("want a record, got nil")
	}
}

func testAssumptionsBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = Assumptions().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testAssumptionsOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := Assumptions().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testAssumptionsAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	assumptionOne := &Assumption{}
	assumptionTwo := &Assumption{}
	if err = randomize.Struct(seed, assumptionOne, assumptionDBTypes, false, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}
	if err = randomize.Struct(seed, assumptionTwo, assumptionDBTypes, false, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = assumptionOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = assumptionTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := Assumptions().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testAssumptionsCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	assumptionOne := &Assumption{}
	assumptionTwo := &Assumption{}
	if err = randomize.Struct(seed, assumptionOne, assumptionDBTypes, false, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}
	if err = randomize.Struct(seed, assumptionTwo, assumptionDBTypes, false, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = assumptionOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = assumptionTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func assumptionBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func assumptionAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *Assumption) error {
	*o = Assumption{}
	return nil
}

func testAssumptionsHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &Assumption{}
	o := &Assumption{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, assumptionDBTypes, false); err != nil {
		t.Errorf("Unable to randomize Assumption object: %s", err)
	}

	AddAssumptionHook(boil.BeforeInsertHook, assumptionBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	assumptionBeforeInsertHooks = []AssumptionHook{}

	AddAssumptionHook(boil.AfterInsertHook, assumptionAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	assumptionAfterInsertHooks = []AssumptionHook{}

	AddAssumptionHook(boil.AfterSelectHook, assumptionAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	assumptionAfterSelectHooks = []AssumptionHook{}

	AddAssumptionHook(boil.BeforeUpdateHook, assumptionBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	assumptionBeforeUpdateHooks = []AssumptionHook{}

	AddAssumptionHook(boil.AfterUpdateHook, assumptionAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	assumptionAfterUpdateHooks = []AssumptionHook{}

	AddAssumptionHook(boil.BeforeDeleteHook, assumptionBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	assumptionBeforeDeleteHooks = []AssumptionHook{}

	AddAssumptionHook(boil.AfterDeleteHook, assumptionAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	assumptionAfterDeleteHooks = []AssumptionHook{}

	AddAssumptionHook(boil.BeforeUpsertHook, assumptionBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	assumptionBeforeUpsertHooks = []AssumptionHook{}

	AddAssumptionHook(boil.AfterUpsertHook, assumptionAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	assumptionAfterUpsertHooks = []AssumptionHook{}
}

func testAssumptionsInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testAssumptionsInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(assumptionColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testAssumptionToManyResponses(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Assumption
	var b, c Response

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, responseDBTypes, false, responseColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, responseDBTypes, false, responseColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.AssumptionID = a.ID
	c.AssumptionID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	response, err := a.Responses().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range response {
		if v.AssumptionID == b.AssumptionID {
			bFound = true
		}
		if v.AssumptionID == c.AssumptionID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := AssumptionSlice{&a}
	if err = a.L.LoadResponses(ctx, tx, false, (*[]*Assumption)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Responses); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.Responses = nil
	if err = a.L.LoadResponses(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Responses); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", response)
	}
}

func testAssumptionToManySurveys(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Assumption
	var b, c Survey

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, surveyDBTypes, false, surveyColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, surveyDBTypes, false, surveyColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	_, err = tx.Exec("insert into \"survey_assumption\" (\"assumption_id\", \"survey_id\") values ($1, $2)", a.ID, b.ID)
	if err != nil {
		t.Fatal(err)
	}
	_, err = tx.Exec("insert into \"survey_assumption\" (\"assumption_id\", \"survey_id\") values ($1, $2)", a.ID, c.ID)
	if err != nil {
		t.Fatal(err)
	}

	survey, err := a.Surveys().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range survey {
		if v.ID == b.ID {
			bFound = true
		}
		if v.ID == c.ID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := AssumptionSlice{&a}
	if err = a.L.LoadSurveys(ctx, tx, false, (*[]*Assumption)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Surveys); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.Surveys = nil
	if err = a.L.LoadSurveys(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Surveys); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", survey)
	}
}

func testAssumptionToManyAddOpResponses(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Assumption
	var b, c, d, e Response

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, assumptionDBTypes, false, strmangle.SetComplement(assumptionPrimaryKeyColumns, assumptionColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Response{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, responseDBTypes, false, strmangle.SetComplement(responsePrimaryKeyColumns, responseColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Response{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddResponses(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.AssumptionID {
			t.Error("foreign key was wrong value", a.ID, first.AssumptionID)
		}
		if a.ID != second.AssumptionID {
			t.Error("foreign key was wrong value", a.ID, second.AssumptionID)
		}

		if first.R.Assumption != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.Assumption != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.Responses[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.Responses[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.Responses().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testAssumptionToManyAddOpSurveys(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Assumption
	var b, c, d, e Survey

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, assumptionDBTypes, false, strmangle.SetComplement(assumptionPrimaryKeyColumns, assumptionColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Survey{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, surveyDBTypes, false, strmangle.SetComplement(surveyPrimaryKeyColumns, surveyColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Survey{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddSurveys(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if first.R.Assumptions[0] != &a {
			t.Error("relationship was not added properly to the slice")
		}
		if second.R.Assumptions[0] != &a {
			t.Error("relationship was not added properly to the slice")
		}

		if a.R.Surveys[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.Surveys[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.Surveys().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}

func testAssumptionToManySetOpSurveys(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Assumption
	var b, c, d, e Survey

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, assumptionDBTypes, false, strmangle.SetComplement(assumptionPrimaryKeyColumns, assumptionColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Survey{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, surveyDBTypes, false, strmangle.SetComplement(surveyPrimaryKeyColumns, surveyColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err = a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	err = a.SetSurveys(ctx, tx, false, &b, &c)
	if err != nil {
		t.Fatal(err)
	}

	count, err := a.Surveys().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Error("count was wrong:", count)
	}

	err = a.SetSurveys(ctx, tx, true, &d, &e)
	if err != nil {
		t.Fatal(err)
	}

	count, err = a.Surveys().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Error("count was wrong:", count)
	}

	// The following checks cannot be implemented since we have no handle
	// to these when we call Set(). Leaving them here as wishful thinking
	// and to let people know there's dragons.
	//
	// if len(b.R.Assumptions) != 0 {
	// 	t.Error("relationship was not removed properly from the slice")
	// }
	// if len(c.R.Assumptions) != 0 {
	// 	t.Error("relationship was not removed properly from the slice")
	// }
	if d.R.Assumptions[0] != &a {
		t.Error("relationship was not added properly to the slice")
	}
	if e.R.Assumptions[0] != &a {
		t.Error("relationship was not added properly to the slice")
	}

	if a.R.Surveys[0] != &d {
		t.Error("relationship struct slice not set to correct value")
	}
	if a.R.Surveys[1] != &e {
		t.Error("relationship struct slice not set to correct value")
	}
}

func testAssumptionToManyRemoveOpSurveys(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Assumption
	var b, c, d, e Survey

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, assumptionDBTypes, false, strmangle.SetComplement(assumptionPrimaryKeyColumns, assumptionColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Survey{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, surveyDBTypes, false, strmangle.SetComplement(surveyPrimaryKeyColumns, surveyColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	err = a.AddSurveys(ctx, tx, true, foreigners...)
	if err != nil {
		t.Fatal(err)
	}

	count, err := a.Surveys().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 4 {
		t.Error("count was wrong:", count)
	}

	err = a.RemoveSurveys(ctx, tx, foreigners[:2]...)
	if err != nil {
		t.Fatal(err)
	}

	count, err = a.Surveys().Count(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Error("count was wrong:", count)
	}

	if len(b.R.Assumptions) != 0 {
		t.Error("relationship was not removed properly from the slice")
	}
	if len(c.R.Assumptions) != 0 {
		t.Error("relationship was not removed properly from the slice")
	}
	if d.R.Assumptions[0] != &a {
		t.Error("relationship was not added properly to the foreign struct")
	}
	if e.R.Assumptions[0] != &a {
		t.Error("relationship was not added properly to the foreign struct")
	}

	if len(a.R.Surveys) != 2 {
		t.Error("should have preserved two relationships")
	}

	// Removal doesn't do a stable deletion for performance so we have to flip the order
	if a.R.Surveys[1] != &d {
		t.Error("relationship to d should have been preserved")
	}
	if a.R.Surveys[0] != &e {
		t.Error("relationship to e should have been preserved")
	}
}

func testAssumptionsReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testAssumptionsReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := AssumptionSlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testAssumptionsSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := Assumptions().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	assumptionDBTypes = map[string]string{`AssumptionText`: `text`, `CreatedDatetime`: `timestamp without time zone`, `ID`: `integer`}
	_                 = bytes.MinRead
)

func testAssumptionsUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(assumptionPrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(assumptionColumns) == len(assumptionPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testAssumptionsSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(assumptionColumns) == len(assumptionPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &Assumption{}
	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, assumptionDBTypes, true, assumptionPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(assumptionColumns, assumptionPrimaryKeyColumns) {
		fields = assumptionColumns
	} else {
		fields = strmangle.SetComplement(
			assumptionColumns,
			assumptionPrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := AssumptionSlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}

func testAssumptionsUpsert(t *testing.T) {
	t.Parallel()

	if len(assumptionColumns) == len(assumptionPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	o := Assumption{}
	if err = randomize.Struct(seed, &o, assumptionDBTypes, true); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Upsert(ctx, tx, false, nil, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert Assumption: %s", err)
	}

	count, err := Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &o, assumptionDBTypes, false, assumptionPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Assumption struct: %s", err)
	}

	if err = o.Upsert(ctx, tx, true, nil, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert Assumption: %s", err)
	}

	count, err = Assumptions().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
