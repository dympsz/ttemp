package datastore

import (
	"context"
	"errors"
	"time"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"

	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
)

func NewResponse(response *models.Response) (int, error) {
	err := response.Insert(context.Background(), db, boil.Infer())
	return response.ID, err
}

func ListResponses() (models.ResponseSlice, error) {
	responses, err := models.Responses().All(context.Background(), db)
	return responses, err
}

func ListResponsesWithFilters(userID *int, surveyID *int, assumptionID *int) (models.ResponseSlice, error) {
	var mods []qm.QueryMod = []qm.QueryMod{}
	mods = append(mods, qm.Select())
	if userID != nil {
		mods = append(mods, qm.Where("app_user_id = ?", *userID))
	}
	if surveyID != nil {
		mods = append(mods, qm.Where("survey_id = ?", *surveyID))
	}
	if assumptionID != nil {
		mods = append(mods, qm.Where("assumption_id = ?", *assumptionID))
	}
	responses, err := models.Responses(mods...).All(context.Background(), db)

	return responses, err
}

/* SQL to find latest per user:
select r.*
from response r
	inner join (
		select app_user_id, max(response_datetime) max_datetime
		from response r
		where survey_id = 1 and assumption_id = 2
		group by app_user_id
	) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
where r.survey_id = 1 and r.assumption_id = 2;
*/
func ListResponsesWithFiltersLatestPerUser(surveyID int, assumptionID int) (models.ResponseSlice, error) {
	sql := `select r.*
	from response r
		inner join (
			select app_user_id, max(response_datetime) max_datetime
			from response r
			where survey_id = $1 and assumption_id = $2
			group by app_user_id
		) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
	where r.survey_id = $3 and r.assumption_id = $4`
	var responses models.ResponseSlice
	err := queries.Raw(sql,
		surveyID, assumptionID, surveyID, assumptionID).Bind(context.Background(), db, &responses)

	return responses, err
}

/* SQL to find latest:
select r.*
from response r
	inner join (
		select app_user_id, max(response_datetime) max_datetime
		from response r
		where app_user_id = 1 and survey_id = 1 and assumption_id = 2
		group by app_user_id
	) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
where r.app_user_id = 1 and r.survey_id = 1 and r.assumption_id = 2;
*/
func ListResponsesWithFiltersLatest(userID int, surveyID int, assumptionID int) (models.Response, error) {
	sql := `select r.*
	from response r
		inner join (
			select app_user_id, max(response_datetime) max_datetime
			from response r
			where app_user_id = $1 and survey_id = $2 and assumption_id = $3
			group by app_user_id
		) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
	where r.app_user_id = $4 and r.survey_id = $5 and r.assumption_id = $6`
	var response models.Response
	err := queries.Raw(sql,
		userID, surveyID, assumptionID, userID, surveyID, assumptionID).Bind(context.Background(), db, &response)

	return response, err
}

/* SQL to find latest per user with before DT:
select r.*
from response r
	inner join (
		select app_user_id, max(response_datetime) max_datetime
		from response r
		where survey_id = 1 and assumption_id = 2
			and r.response_datetime < '2019-01-02'
		group by app_user_id
	) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
where r.survey_id = 1 and r.assumption_id = 2;
*/
func ListResponsesWithFiltersLatestPerUserBeforeDT(surveyID int, assumptionID int, beforeDateTime time.Time) (models.ResponseSlice, error) {
	sql := `select r.*
	from response r
		inner join (
			select app_user_id, max(response_datetime) max_datetime
			from response r
			where survey_id = $1 and assumption_id = $2
				and r.response_datetime < $3
			group by app_user_id
		) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
	where r.survey_id = $4 and r.assumption_id = $5`
	var responses models.ResponseSlice
	err := queries.Raw(sql,
		surveyID, assumptionID, beforeDateTime, surveyID, assumptionID).Bind(context.Background(), db, &responses)

	return responses, err
}

/* SQL to find latest with before DT:
select r.*
from response r
	inner join (
		select app_user_id, max(response_datetime) max_datetime
		from response r
		where app_user_id = 1 and survey_id = 1 and assumption_id = 2
			and r.response_datetime < '2019-01-02'
		group by app_user_id
	) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
where r.app_user_id = 1 and r.survey_id = 1 and r.assumption_id = 2;
*/
func ListResponsesWithFiltersLatestBeforeDT(userID int, surveyID int, assumptionID int, beforeDateTime time.Time) (models.Response, error) {
	sql := `select r.*
	from response r
		inner join (
			select app_user_id, max(response_datetime) max_datetime
			from response r
			where app_user_id = $1 and survey_id = $2 and assumption_id = $3
				and r.response_datetime < $4
			group by app_user_id
		) t on r.app_user_id = t.app_user_id and r.response_datetime = t.max_datetime
	where r.app_user_id = $5 and r.survey_id = $6 and r.assumption_id = $7`
	var response models.Response
	err := queries.Raw(sql,
		userID, surveyID, assumptionID, beforeDateTime, userID, surveyID, assumptionID).Bind(context.Background(), db, &response)

	return response, err
}

func GetResponse(id int) (*models.Response, error) {
	response, err := models.Responses(qm.Where("id = ?", id)).One(context.Background(), db)
	if err != nil {
		return nil, err
	} else if response == nil {
		return nil, errors.New("response not found")
	} else {
		return response, nil
	}
}

func UpdateResponse(response *models.Response) error {
	_, err := response.Update(context.Background(), db, boil.Infer())
	return err
}

func DeleteResponse(response *models.Response) error {
	_, err := response.Delete(context.Background(), db)
	return err
}
