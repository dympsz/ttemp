// Code generated by SQLBoiler (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/volatiletech/null"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"github.com/volatiletech/sqlboiler/strmangle"
)

// AppUser is an object representing the database table.
type AppUser struct {
	ID           int         `boil:"id" json:"id" toml:"id" yaml:"id"`
	Login        string      `boil:"login" json:"login" toml:"login" yaml:"login"`
	PasswordHash null.Bytes  `boil:"password_hash" json:"password_hash,omitempty" toml:"password_hash" yaml:"password_hash,omitempty"`
	Email        null.String `boil:"email" json:"email,omitempty" toml:"email" yaml:"email,omitempty"`
	FullName     null.String `boil:"full_name" json:"full_name,omitempty" toml:"full_name" yaml:"full_name,omitempty"`
	UserUUID     string      `boil:"user_uuid" json:"user_uuid" toml:"user_uuid" yaml:"user_uuid"`
	IsAdmin      bool        `boil:"is_admin" json:"is_admin" toml:"is_admin" yaml:"is_admin"`

	R *appUserR `boil:"-" json:"-" toml:"-" yaml:"-"`
	L appUserL  `boil:"-" json:"-" toml:"-" yaml:"-"`
}

var AppUserColumns = struct {
	ID           string
	Login        string
	PasswordHash string
	Email        string
	FullName     string
	UserUUID     string
	IsAdmin      string
}{
	ID:           "id",
	Login:        "login",
	PasswordHash: "password_hash",
	Email:        "email",
	FullName:     "full_name",
	UserUUID:     "user_uuid",
	IsAdmin:      "is_admin",
}

// AppUserRels is where relationship names are stored.
var AppUserRels = struct {
	Surveys   string
	Responses string
}{
	Surveys:   "Surveys",
	Responses: "Responses",
}

// appUserR is where relationships are stored.
type appUserR struct {
	Surveys   SurveySlice
	Responses ResponseSlice
}

// NewStruct creates a new relationship struct
func (*appUserR) NewStruct() *appUserR {
	return &appUserR{}
}

// appUserL is where Load methods for each relationship are stored.
type appUserL struct{}

var (
	appUserColumns               = []string{"id", "login", "password_hash", "email", "full_name", "user_uuid", "is_admin"}
	appUserColumnsWithoutDefault = []string{"login", "password_hash", "email", "full_name"}
	appUserColumnsWithDefault    = []string{"id", "user_uuid", "is_admin"}
	appUserPrimaryKeyColumns     = []string{"id"}
)

type (
	// AppUserSlice is an alias for a slice of pointers to AppUser.
	// This should generally be used opposed to []AppUser.
	AppUserSlice []*AppUser
	// AppUserHook is the signature for custom AppUser hook methods
	AppUserHook func(context.Context, boil.ContextExecutor, *AppUser) error

	appUserQuery struct {
		*queries.Query
	}
)

// Cache for insert, update and upsert
var (
	appUserType                 = reflect.TypeOf(&AppUser{})
	appUserMapping              = queries.MakeStructMapping(appUserType)
	appUserPrimaryKeyMapping, _ = queries.BindMapping(appUserType, appUserMapping, appUserPrimaryKeyColumns)
	appUserInsertCacheMut       sync.RWMutex
	appUserInsertCache          = make(map[string]insertCache)
	appUserUpdateCacheMut       sync.RWMutex
	appUserUpdateCache          = make(map[string]updateCache)
	appUserUpsertCacheMut       sync.RWMutex
	appUserUpsertCache          = make(map[string]insertCache)
)

var (
	// Force time package dependency for automated UpdatedAt/CreatedAt.
	_ = time.Second
)

var appUserBeforeInsertHooks []AppUserHook
var appUserBeforeUpdateHooks []AppUserHook
var appUserBeforeDeleteHooks []AppUserHook
var appUserBeforeUpsertHooks []AppUserHook

var appUserAfterInsertHooks []AppUserHook
var appUserAfterSelectHooks []AppUserHook
var appUserAfterUpdateHooks []AppUserHook
var appUserAfterDeleteHooks []AppUserHook
var appUserAfterUpsertHooks []AppUserHook

// doBeforeInsertHooks executes all "before insert" hooks.
func (o *AppUser) doBeforeInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserBeforeInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpdateHooks executes all "before Update" hooks.
func (o *AppUser) doBeforeUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserBeforeUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeDeleteHooks executes all "before Delete" hooks.
func (o *AppUser) doBeforeDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserBeforeDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpsertHooks executes all "before Upsert" hooks.
func (o *AppUser) doBeforeUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserBeforeUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterInsertHooks executes all "after Insert" hooks.
func (o *AppUser) doAfterInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserAfterInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterSelectHooks executes all "after Select" hooks.
func (o *AppUser) doAfterSelectHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserAfterSelectHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpdateHooks executes all "after Update" hooks.
func (o *AppUser) doAfterUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserAfterUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterDeleteHooks executes all "after Delete" hooks.
func (o *AppUser) doAfterDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserAfterDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpsertHooks executes all "after Upsert" hooks.
func (o *AppUser) doAfterUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	for _, hook := range appUserAfterUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// AddAppUserHook registers your hook function for all future operations.
func AddAppUserHook(hookPoint boil.HookPoint, appUserHook AppUserHook) {
	switch hookPoint {
	case boil.BeforeInsertHook:
		appUserBeforeInsertHooks = append(appUserBeforeInsertHooks, appUserHook)
	case boil.BeforeUpdateHook:
		appUserBeforeUpdateHooks = append(appUserBeforeUpdateHooks, appUserHook)
	case boil.BeforeDeleteHook:
		appUserBeforeDeleteHooks = append(appUserBeforeDeleteHooks, appUserHook)
	case boil.BeforeUpsertHook:
		appUserBeforeUpsertHooks = append(appUserBeforeUpsertHooks, appUserHook)
	case boil.AfterInsertHook:
		appUserAfterInsertHooks = append(appUserAfterInsertHooks, appUserHook)
	case boil.AfterSelectHook:
		appUserAfterSelectHooks = append(appUserAfterSelectHooks, appUserHook)
	case boil.AfterUpdateHook:
		appUserAfterUpdateHooks = append(appUserAfterUpdateHooks, appUserHook)
	case boil.AfterDeleteHook:
		appUserAfterDeleteHooks = append(appUserAfterDeleteHooks, appUserHook)
	case boil.AfterUpsertHook:
		appUserAfterUpsertHooks = append(appUserAfterUpsertHooks, appUserHook)
	}
}

// One returns a single appUser record from the query.
func (q appUserQuery) One(ctx context.Context, exec boil.ContextExecutor) (*AppUser, error) {
	o := &AppUser{}

	queries.SetLimit(q.Query, 1)

	err := q.Bind(ctx, exec, o)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: failed to execute a one query for app_user")
	}

	if err := o.doAfterSelectHooks(ctx, exec); err != nil {
		return o, err
	}

	return o, nil
}

// All returns all AppUser records from the query.
func (q appUserQuery) All(ctx context.Context, exec boil.ContextExecutor) (AppUserSlice, error) {
	var o []*AppUser

	err := q.Bind(ctx, exec, &o)
	if err != nil {
		return nil, errors.Wrap(err, "models: failed to assign all query results to AppUser slice")
	}

	if len(appUserAfterSelectHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterSelectHooks(ctx, exec); err != nil {
				return o, err
			}
		}
	}

	return o, nil
}

// Count returns the count of all AppUser records in the query.
func (q appUserQuery) Count(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to count app_user rows")
	}

	return count, nil
}

// Exists checks if the row exists in the table.
func (q appUserQuery) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)
	queries.SetLimit(q.Query, 1)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return false, errors.Wrap(err, "models: failed to check if app_user exists")
	}

	return count > 0, nil
}

// Surveys retrieves all the survey's Surveys with an executor.
func (o *AppUser) Surveys(mods ...qm.QueryMod) surveyQuery {
	var queryMods []qm.QueryMod
	if len(mods) != 0 {
		queryMods = append(queryMods, mods...)
	}

	queryMods = append(queryMods,
		qm.InnerJoin("\"app_user_survey\" on \"survey\".\"id\" = \"app_user_survey\".\"survey_id\""),
		qm.Where("\"app_user_survey\".\"app_user_id\"=?", o.ID),
	)

	query := Surveys(queryMods...)
	queries.SetFrom(query.Query, "\"survey\"")

	if len(queries.GetSelect(query.Query)) == 0 {
		queries.SetSelect(query.Query, []string{"\"survey\".*"})
	}

	return query
}

// Responses retrieves all the response's Responses with an executor.
func (o *AppUser) Responses(mods ...qm.QueryMod) responseQuery {
	var queryMods []qm.QueryMod
	if len(mods) != 0 {
		queryMods = append(queryMods, mods...)
	}

	queryMods = append(queryMods,
		qm.Where("\"response\".\"app_user_id\"=?", o.ID),
	)

	query := Responses(queryMods...)
	queries.SetFrom(query.Query, "\"response\"")

	if len(queries.GetSelect(query.Query)) == 0 {
		queries.SetSelect(query.Query, []string{"\"response\".*"})
	}

	return query
}

// LoadSurveys allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for a 1-M or N-M relationship.
func (appUserL) LoadSurveys(ctx context.Context, e boil.ContextExecutor, singular bool, maybeAppUser interface{}, mods queries.Applicator) error {
	var slice []*AppUser
	var object *AppUser

	if singular {
		object = maybeAppUser.(*AppUser)
	} else {
		slice = *maybeAppUser.(*[]*AppUser)
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &appUserR{}
		}
		args = append(args, object.ID)
	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &appUserR{}
			}

			for _, a := range args {
				if a == obj.ID {
					continue Outer
				}
			}

			args = append(args, obj.ID)
		}
	}

	query := NewQuery(
		qm.Select("\"survey\".*, \"a\".\"app_user_id\""),
		qm.From("\"survey\""),
		qm.InnerJoin("\"app_user_survey\" as \"a\" on \"survey\".\"id\" = \"a\".\"survey_id\""),
		qm.WhereIn("\"a\".\"app_user_id\" in ?", args...),
	)
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load survey")
	}

	var resultSlice []*Survey

	var localJoinCols []int
	for results.Next() {
		one := new(Survey)
		var localJoinCol int

		err = results.Scan(&one.ID, &one.Name, &one.StartDatetime, &one.EndDatetime, &localJoinCol)
		if err != nil {
			return errors.Wrap(err, "failed to scan eager loaded results for survey")
		}
		if err = results.Err(); err != nil {
			return errors.Wrap(err, "failed to plebian-bind eager loaded slice survey")
		}

		resultSlice = append(resultSlice, one)
		localJoinCols = append(localJoinCols, localJoinCol)
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results in eager load on survey")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for survey")
	}

	if len(surveyAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}
	if singular {
		object.R.Surveys = resultSlice
		for _, foreign := range resultSlice {
			if foreign.R == nil {
				foreign.R = &surveyR{}
			}
			foreign.R.AppUsers = append(foreign.R.AppUsers, object)
		}
		return nil
	}

	for i, foreign := range resultSlice {
		localJoinCol := localJoinCols[i]
		for _, local := range slice {
			if local.ID == localJoinCol {
				local.R.Surveys = append(local.R.Surveys, foreign)
				if foreign.R == nil {
					foreign.R = &surveyR{}
				}
				foreign.R.AppUsers = append(foreign.R.AppUsers, local)
				break
			}
		}
	}

	return nil
}

// LoadResponses allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for a 1-M or N-M relationship.
func (appUserL) LoadResponses(ctx context.Context, e boil.ContextExecutor, singular bool, maybeAppUser interface{}, mods queries.Applicator) error {
	var slice []*AppUser
	var object *AppUser

	if singular {
		object = maybeAppUser.(*AppUser)
	} else {
		slice = *maybeAppUser.(*[]*AppUser)
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &appUserR{}
		}
		args = append(args, object.ID)
	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &appUserR{}
			}

			for _, a := range args {
				if a == obj.ID {
					continue Outer
				}
			}

			args = append(args, obj.ID)
		}
	}

	query := NewQuery(qm.From(`response`), qm.WhereIn(`app_user_id in ?`, args...))
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load response")
	}

	var resultSlice []*Response
	if err = queries.Bind(results, &resultSlice); err != nil {
		return errors.Wrap(err, "failed to bind eager loaded slice response")
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results in eager load on response")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for response")
	}

	if len(responseAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}
	if singular {
		object.R.Responses = resultSlice
		for _, foreign := range resultSlice {
			if foreign.R == nil {
				foreign.R = &responseR{}
			}
			foreign.R.AppUser = object
		}
		return nil
	}

	for _, foreign := range resultSlice {
		for _, local := range slice {
			if local.ID == foreign.AppUserID {
				local.R.Responses = append(local.R.Responses, foreign)
				if foreign.R == nil {
					foreign.R = &responseR{}
				}
				foreign.R.AppUser = local
				break
			}
		}
	}

	return nil
}

// AddSurveys adds the given related objects to the existing relationships
// of the app_user, optionally inserting them as new records.
// Appends related to o.R.Surveys.
// Sets related.R.AppUsers appropriately.
func (o *AppUser) AddSurveys(ctx context.Context, exec boil.ContextExecutor, insert bool, related ...*Survey) error {
	var err error
	for _, rel := range related {
		if insert {
			if err = rel.Insert(ctx, exec, boil.Infer()); err != nil {
				return errors.Wrap(err, "failed to insert into foreign table")
			}
		}
	}

	for _, rel := range related {
		query := "insert into \"app_user_survey\" (\"app_user_id\", \"survey_id\") values ($1, $2)"
		values := []interface{}{o.ID, rel.ID}

		if boil.DebugMode {
			fmt.Fprintln(boil.DebugWriter, query)
			fmt.Fprintln(boil.DebugWriter, values)
		}

		_, err = exec.ExecContext(ctx, query, values...)
		if err != nil {
			return errors.Wrap(err, "failed to insert into join table")
		}
	}
	if o.R == nil {
		o.R = &appUserR{
			Surveys: related,
		}
	} else {
		o.R.Surveys = append(o.R.Surveys, related...)
	}

	for _, rel := range related {
		if rel.R == nil {
			rel.R = &surveyR{
				AppUsers: AppUserSlice{o},
			}
		} else {
			rel.R.AppUsers = append(rel.R.AppUsers, o)
		}
	}
	return nil
}

// SetSurveys removes all previously related items of the
// app_user replacing them completely with the passed
// in related items, optionally inserting them as new records.
// Sets o.R.AppUsers's Surveys accordingly.
// Replaces o.R.Surveys with related.
// Sets related.R.AppUsers's Surveys accordingly.
func (o *AppUser) SetSurveys(ctx context.Context, exec boil.ContextExecutor, insert bool, related ...*Survey) error {
	query := "delete from \"app_user_survey\" where \"app_user_id\" = $1"
	values := []interface{}{o.ID}
	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, query)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	_, err := exec.ExecContext(ctx, query, values...)
	if err != nil {
		return errors.Wrap(err, "failed to remove relationships before set")
	}

	removeSurveysFromAppUsersSlice(o, related)
	if o.R != nil {
		o.R.Surveys = nil
	}
	return o.AddSurveys(ctx, exec, insert, related...)
}

// RemoveSurveys relationships from objects passed in.
// Removes related items from R.Surveys (uses pointer comparison, removal does not keep order)
// Sets related.R.AppUsers.
func (o *AppUser) RemoveSurveys(ctx context.Context, exec boil.ContextExecutor, related ...*Survey) error {
	var err error
	query := fmt.Sprintf(
		"delete from \"app_user_survey\" where \"app_user_id\" = $1 and \"survey_id\" in (%s)",
		strmangle.Placeholders(dialect.UseIndexPlaceholders, len(related), 2, 1),
	)
	values := []interface{}{o.ID}
	for _, rel := range related {
		values = append(values, rel.ID)
	}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, query)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	_, err = exec.ExecContext(ctx, query, values...)
	if err != nil {
		return errors.Wrap(err, "failed to remove relationships before set")
	}
	removeSurveysFromAppUsersSlice(o, related)
	if o.R == nil {
		return nil
	}

	for _, rel := range related {
		for i, ri := range o.R.Surveys {
			if rel != ri {
				continue
			}

			ln := len(o.R.Surveys)
			if ln > 1 && i < ln-1 {
				o.R.Surveys[i] = o.R.Surveys[ln-1]
			}
			o.R.Surveys = o.R.Surveys[:ln-1]
			break
		}
	}

	return nil
}

func removeSurveysFromAppUsersSlice(o *AppUser, related []*Survey) {
	for _, rel := range related {
		if rel.R == nil {
			continue
		}
		for i, ri := range rel.R.AppUsers {
			if o.ID != ri.ID {
				continue
			}

			ln := len(rel.R.AppUsers)
			if ln > 1 && i < ln-1 {
				rel.R.AppUsers[i] = rel.R.AppUsers[ln-1]
			}
			rel.R.AppUsers = rel.R.AppUsers[:ln-1]
			break
		}
	}
}

// AddResponses adds the given related objects to the existing relationships
// of the app_user, optionally inserting them as new records.
// Appends related to o.R.Responses.
// Sets related.R.AppUser appropriately.
func (o *AppUser) AddResponses(ctx context.Context, exec boil.ContextExecutor, insert bool, related ...*Response) error {
	var err error
	for _, rel := range related {
		if insert {
			rel.AppUserID = o.ID
			if err = rel.Insert(ctx, exec, boil.Infer()); err != nil {
				return errors.Wrap(err, "failed to insert into foreign table")
			}
		} else {
			updateQuery := fmt.Sprintf(
				"UPDATE \"response\" SET %s WHERE %s",
				strmangle.SetParamNames("\"", "\"", 1, []string{"app_user_id"}),
				strmangle.WhereClause("\"", "\"", 2, responsePrimaryKeyColumns),
			)
			values := []interface{}{o.ID, rel.ID}

			if boil.DebugMode {
				fmt.Fprintln(boil.DebugWriter, updateQuery)
				fmt.Fprintln(boil.DebugWriter, values)
			}

			if _, err = exec.ExecContext(ctx, updateQuery, values...); err != nil {
				return errors.Wrap(err, "failed to update foreign table")
			}

			rel.AppUserID = o.ID
		}
	}

	if o.R == nil {
		o.R = &appUserR{
			Responses: related,
		}
	} else {
		o.R.Responses = append(o.R.Responses, related...)
	}

	for _, rel := range related {
		if rel.R == nil {
			rel.R = &responseR{
				AppUser: o,
			}
		} else {
			rel.R.AppUser = o
		}
	}
	return nil
}

// AppUsers retrieves all the records using an executor.
func AppUsers(mods ...qm.QueryMod) appUserQuery {
	mods = append(mods, qm.From("\"app_user\""))
	return appUserQuery{NewQuery(mods...)}
}

// FindAppUser retrieves a single record by ID with an executor.
// If selectCols is empty Find will return all columns.
func FindAppUser(ctx context.Context, exec boil.ContextExecutor, iD int, selectCols ...string) (*AppUser, error) {
	appUserObj := &AppUser{}

	sel := "*"
	if len(selectCols) > 0 {
		sel = strings.Join(strmangle.IdentQuoteSlice(dialect.LQ, dialect.RQ, selectCols), ",")
	}
	query := fmt.Sprintf(
		"select %s from \"app_user\" where \"id\"=$1", sel,
	)

	q := queries.Raw(query, iD)

	err := q.Bind(ctx, exec, appUserObj)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: unable to select from app_user")
	}

	return appUserObj, nil
}

// Insert a single record using an executor.
// See boil.Columns.InsertColumnSet documentation to understand column list inference for inserts.
func (o *AppUser) Insert(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) error {
	if o == nil {
		return errors.New("models: no app_user provided for insertion")
	}

	var err error

	if err := o.doBeforeInsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(appUserColumnsWithDefault, o)

	key := makeCacheKey(columns, nzDefaults)
	appUserInsertCacheMut.RLock()
	cache, cached := appUserInsertCache[key]
	appUserInsertCacheMut.RUnlock()

	if !cached {
		wl, returnColumns := columns.InsertColumnSet(
			appUserColumns,
			appUserColumnsWithDefault,
			appUserColumnsWithoutDefault,
			nzDefaults,
		)

		cache.valueMapping, err = queries.BindMapping(appUserType, appUserMapping, wl)
		if err != nil {
			return err
		}
		cache.retMapping, err = queries.BindMapping(appUserType, appUserMapping, returnColumns)
		if err != nil {
			return err
		}
		if len(wl) != 0 {
			cache.query = fmt.Sprintf("INSERT INTO \"app_user\" (\"%s\") %%sVALUES (%s)%%s", strings.Join(wl, "\",\""), strmangle.Placeholders(dialect.UseIndexPlaceholders, len(wl), 1, 1))
		} else {
			cache.query = "INSERT INTO \"app_user\" %sDEFAULT VALUES%s"
		}

		var queryOutput, queryReturning string

		if len(cache.retMapping) != 0 {
			queryReturning = fmt.Sprintf(" RETURNING \"%s\"", strings.Join(returnColumns, "\",\""))
		}

		cache.query = fmt.Sprintf(cache.query, queryOutput, queryReturning)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, vals)
	}

	if len(cache.retMapping) != 0 {
		err = exec.QueryRowContext(ctx, cache.query, vals...).Scan(queries.PtrsFromMapping(value, cache.retMapping)...)
	} else {
		_, err = exec.ExecContext(ctx, cache.query, vals...)
	}

	if err != nil {
		return errors.Wrap(err, "models: unable to insert into app_user")
	}

	if !cached {
		appUserInsertCacheMut.Lock()
		appUserInsertCache[key] = cache
		appUserInsertCacheMut.Unlock()
	}

	return o.doAfterInsertHooks(ctx, exec)
}

// Update uses an executor to update the AppUser.
// See boil.Columns.UpdateColumnSet documentation to understand column list inference for updates.
// Update does not automatically update the record in case of default values. Use .Reload() to refresh the records.
func (o *AppUser) Update(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) (int64, error) {
	var err error
	if err = o.doBeforeUpdateHooks(ctx, exec); err != nil {
		return 0, err
	}
	key := makeCacheKey(columns, nil)
	appUserUpdateCacheMut.RLock()
	cache, cached := appUserUpdateCache[key]
	appUserUpdateCacheMut.RUnlock()

	if !cached {
		wl := columns.UpdateColumnSet(
			appUserColumns,
			appUserPrimaryKeyColumns,
		)

		if !columns.IsWhitelist() {
			wl = strmangle.SetComplement(wl, []string{"created_at"})
		}
		if len(wl) == 0 {
			return 0, errors.New("models: unable to update app_user, could not build whitelist")
		}

		cache.query = fmt.Sprintf("UPDATE \"app_user\" SET %s WHERE %s",
			strmangle.SetParamNames("\"", "\"", 1, wl),
			strmangle.WhereClause("\"", "\"", len(wl)+1, appUserPrimaryKeyColumns),
		)
		cache.valueMapping, err = queries.BindMapping(appUserType, appUserMapping, append(wl, appUserPrimaryKeyColumns...))
		if err != nil {
			return 0, err
		}
	}

	values := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), cache.valueMapping)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	var result sql.Result
	result, err = exec.ExecContext(ctx, cache.query, values...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update app_user row")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by update for app_user")
	}

	if !cached {
		appUserUpdateCacheMut.Lock()
		appUserUpdateCache[key] = cache
		appUserUpdateCacheMut.Unlock()
	}

	return rowsAff, o.doAfterUpdateHooks(ctx, exec)
}

// UpdateAll updates all rows with the specified column values.
func (q appUserQuery) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	queries.SetUpdate(q.Query, cols)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all for app_user")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected for app_user")
	}

	return rowsAff, nil
}

// UpdateAll updates all rows with the specified column values, using an executor.
func (o AppUserSlice) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	ln := int64(len(o))
	if ln == 0 {
		return 0, nil
	}

	if len(cols) == 0 {
		return 0, errors.New("models: update all requires at least one column argument")
	}

	colNames := make([]string, len(cols))
	args := make([]interface{}, len(cols))

	i := 0
	for name, value := range cols {
		colNames[i] = name
		args[i] = value
		i++
	}

	// Append all of the primary key values for each column
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), appUserPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := fmt.Sprintf("UPDATE \"app_user\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 1, colNames),
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), len(colNames)+1, appUserPrimaryKeyColumns, len(o)))

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args...)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all in appUser slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected all in update all appUser")
	}
	return rowsAff, nil
}

// Upsert attempts an insert using an executor, and does an update or ignore on conflict.
// See boil.Columns documentation for how to properly use updateColumns and insertColumns.
func (o *AppUser) Upsert(ctx context.Context, exec boil.ContextExecutor, updateOnConflict bool, conflictColumns []string, updateColumns, insertColumns boil.Columns) error {
	if o == nil {
		return errors.New("models: no app_user provided for upsert")
	}

	if err := o.doBeforeUpsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(appUserColumnsWithDefault, o)

	// Build cache key in-line uglily - mysql vs psql problems
	buf := strmangle.GetBuffer()
	if updateOnConflict {
		buf.WriteByte('t')
	} else {
		buf.WriteByte('f')
	}
	buf.WriteByte('.')
	for _, c := range conflictColumns {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(updateColumns.Kind))
	for _, c := range updateColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(insertColumns.Kind))
	for _, c := range insertColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	for _, c := range nzDefaults {
		buf.WriteString(c)
	}
	key := buf.String()
	strmangle.PutBuffer(buf)

	appUserUpsertCacheMut.RLock()
	cache, cached := appUserUpsertCache[key]
	appUserUpsertCacheMut.RUnlock()

	var err error

	if !cached {
		insert, ret := insertColumns.InsertColumnSet(
			appUserColumns,
			appUserColumnsWithDefault,
			appUserColumnsWithoutDefault,
			nzDefaults,
		)
		update := updateColumns.UpdateColumnSet(
			appUserColumns,
			appUserPrimaryKeyColumns,
		)

		if len(update) == 0 {
			return errors.New("models: unable to upsert app_user, could not build update column list")
		}

		conflict := conflictColumns
		if len(conflict) == 0 {
			conflict = make([]string, len(appUserPrimaryKeyColumns))
			copy(conflict, appUserPrimaryKeyColumns)
		}
		cache.query = buildUpsertQueryPostgres(dialect, "\"app_user\"", updateOnConflict, ret, update, conflict, insert)

		cache.valueMapping, err = queries.BindMapping(appUserType, appUserMapping, insert)
		if err != nil {
			return err
		}
		if len(ret) != 0 {
			cache.retMapping, err = queries.BindMapping(appUserType, appUserMapping, ret)
			if err != nil {
				return err
			}
		}
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)
	var returns []interface{}
	if len(cache.retMapping) != 0 {
		returns = queries.PtrsFromMapping(value, cache.retMapping)
	}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, vals)
	}

	if len(cache.retMapping) != 0 {
		err = exec.QueryRowContext(ctx, cache.query, vals...).Scan(returns...)
		if err == sql.ErrNoRows {
			err = nil // Postgres doesn't return anything when there's no update
		}
	} else {
		_, err = exec.ExecContext(ctx, cache.query, vals...)
	}
	if err != nil {
		return errors.Wrap(err, "models: unable to upsert app_user")
	}

	if !cached {
		appUserUpsertCacheMut.Lock()
		appUserUpsertCache[key] = cache
		appUserUpsertCacheMut.Unlock()
	}

	return o.doAfterUpsertHooks(ctx, exec)
}

// Delete deletes a single AppUser record with an executor.
// Delete will match against the primary key column to find the record to delete.
func (o *AppUser) Delete(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if o == nil {
		return 0, errors.New("models: no AppUser provided for delete")
	}

	if err := o.doBeforeDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	args := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), appUserPrimaryKeyMapping)
	sql := "DELETE FROM \"app_user\" WHERE \"id\"=$1"

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args...)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete from app_user")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by delete for app_user")
	}

	if err := o.doAfterDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	return rowsAff, nil
}

// DeleteAll deletes all matching rows.
func (q appUserQuery) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if q.Query == nil {
		return 0, errors.New("models: no appUserQuery provided for delete all")
	}

	queries.SetDelete(q.Query)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from app_user")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for app_user")
	}

	return rowsAff, nil
}

// DeleteAll deletes all rows in the slice, using an executor.
func (o AppUserSlice) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if o == nil {
		return 0, errors.New("models: no AppUser slice provided for delete all")
	}

	if len(o) == 0 {
		return 0, nil
	}

	if len(appUserBeforeDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doBeforeDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	var args []interface{}
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), appUserPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "DELETE FROM \"app_user\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 1, appUserPrimaryKeyColumns, len(o))

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from appUser slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for app_user")
	}

	if len(appUserAfterDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	return rowsAff, nil
}

// Reload refetches the object from the database
// using the primary keys with an executor.
func (o *AppUser) Reload(ctx context.Context, exec boil.ContextExecutor) error {
	ret, err := FindAppUser(ctx, exec, o.ID)
	if err != nil {
		return err
	}

	*o = *ret
	return nil
}

// ReloadAll refetches every row with matching primary key column values
// and overwrites the original object slice with the newly updated slice.
func (o *AppUserSlice) ReloadAll(ctx context.Context, exec boil.ContextExecutor) error {
	if o == nil || len(*o) == 0 {
		return nil
	}

	slice := AppUserSlice{}
	var args []interface{}
	for _, obj := range *o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), appUserPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "SELECT \"app_user\".* FROM \"app_user\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 1, appUserPrimaryKeyColumns, len(*o))

	q := queries.Raw(sql, args...)

	err := q.Bind(ctx, exec, &slice)
	if err != nil {
		return errors.Wrap(err, "models: unable to reload all in AppUserSlice")
	}

	*o = slice

	return nil
}

// AppUserExists checks if the AppUser row exists.
func AppUserExists(ctx context.Context, exec boil.ContextExecutor, iD int) (bool, error) {
	var exists bool
	sql := "select exists(select 1 from \"app_user\" where \"id\"=$1 limit 1)"

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, iD)
	}

	row := exec.QueryRowContext(ctx, sql, iD)

	err := row.Scan(&exists)
	if err != nil {
		return false, errors.Wrap(err, "models: unable to check if app_user exists")
	}

	return exists, nil
}
