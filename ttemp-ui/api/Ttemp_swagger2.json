{
  "swagger": "2.0",
  "info": {
    "contact": {
      "email": "dpszenicyn@gmail.com",
      "name": "Dymitr Pszenicyn",
      "url": "https://gitlab.com/dympsz"
    },
    "description": "Team Temperature API",
    "title": "Ttemp API",
    "version": "1.2.4"
  },
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "basePath": "/api",
  "securityDefinitions": {
    "AuthToken": {
      "type": "apiKey",
      "in": "header",
      "name": "X-auth-token"
    }
  },
  "security": [
    {
      "AuthToken": []
    }
  ],
  "parameters": {
    "requestId": {
      "name": "X-Request-Id",
      "description": "A unique UUID for the request",
      "in": "header",
      "type": "string",
      "required": false,
      "minLength": 1
    }
  },
  "responses": {
    "errorNotFound": {
      "description": "Error - Not found",
      "headers": {
        "X-Request-Id": {
          "description": "The request id this is a response to",
          "type": "string"
        }
      },
      "schema": {
        "$ref": "#/definitions/Error"
      }
    },
    "errorResponse": {
      "description": "Error",
      "headers": {
        "X-Request-Id": {
          "description": "The request id this is a response to",
          "type": "string"
        }
      },
      "schema": {
        "$ref": "#/definitions/Error"
      }
    },
    "errorUnauthorized": {
      "description": "Error - Authentication information is missing or invalid",
      "headers": {
        "X-Request-Id": {
          "description": "The request id this is a response to",
          "type": "string"
        },
        "WWW-Authenticate": {
          "type": "string",
          "description": "Info about auth type and realm"
        }
      },
      "schema": {
        "$ref": "#/definitions/Error"
      }
    }
  },
  "paths": {
    "/login": {
      "post": {
        "parameters": [
          {
            "in": "header",
            "name": "login",
            "required": true,
            "type": "string",
            "minLength": 1
          },
          {
            "in": "header",
            "name": "password",
            "required": true,
            "type": "string",
            "minLength": 1
          }
        ],
        "responses": {
          "200": {
            "description": "Log in successful",
            "headers": {
              "X-auth-token": {
                "type": "string",
                "description": "Authentication token"
              }
            }
          },
          "401": {
            "$ref": "#/responses/errorUnauthorized"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Log in to system",
        "security": []
      }
    },
    "/assumptions": {
      "get": {
        "parameters": [
          {
            "description": "Filter by survey ID",
            "in": "query",
            "name": "surveyId",
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "List of assumptions.",
            "schema": {
              "items": {
                "$ref": "#/definitions/Assumption"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List assumptions"
      },
      "post": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Assumption"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Assumption created",
            "schema": {
              "$ref": "#/definitions/Assumption"
            }
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Create assumption"
      }
    },
    "/assumptions/{assumptionId}": {
      "parameters": [
        {
          "description": "Assumption ID",
          "in": "path",
          "name": "assumptionId",
          "required": true,
          "type": "integer"
        }
      ],
      "delete": {
        "parameters": [],
        "responses": {
          "204": {
            "description": "Assumption deleted"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Delete an assumption"
      },
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "Assumption found",
            "schema": {
              "$ref": "#/definitions/Assumption"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Get an assumption"
      },
      "put": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Assumption"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "Assumption updated"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Update an assumption"
      }
    },
    "/responses": {
      "get": {
        "parameters": [
          {
            "description": "Filter by user ID",
            "in": "query",
            "name": "userId",
            "type": "integer"
          },
          {
            "description": "Filter by survey ID",
            "in": "query",
            "name": "surveyId",
            "type": "integer"
          },
          {
            "description": "Filter by assumption ID",
            "in": "query",
            "name": "assumptionId",
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "List of responses",
            "schema": {
              "items": {
                "$ref": "#/definitions/Response"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List responses"
      },
      "post": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Response"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Response created",
            "schema": {
              "$ref": "#/definitions/Response"
            }
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Create response"
      }
    },
    "/responses/{responseId}": {
      "parameters": [
        {
          "in": "path",
          "name": "responseId",
          "required": true,
          "type": "integer"
        }
      ],
      "delete": {
        "parameters": [],
        "responses": {
          "204": {
            "description": "Response deleted"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Delete response"
      },
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "Response found",
            "schema": {
              "$ref": "#/definitions/Response"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Get response"
      },
      "put": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Response"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "Response updated"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Update response"
      }
    },
    "/surveys": {
      "get": {
        "parameters": [
          {
            "description": "Filter by user ID",
            "in": "query",
            "name": "userId",
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "List of surveys",
            "schema": {
              "items": {
                "$ref": "#/definitions/Survey"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List surveys"
      },
      "post": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Survey"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Survey created",
            "schema": {
              "$ref": "#/definitions/Survey"
            }
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Create survey"
      }
    },
    "/surveys/{surveyId}": {
      "parameters": [
        {
          "in": "path",
          "name": "surveyId",
          "required": true,
          "type": "integer"
        }
      ],
      "delete": {
        "parameters": [],
        "responses": {
          "204": {
            "description": "Survey deleted"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Delete survey"
      },
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "Get survey",
            "schema": {
              "$ref": "#/definitions/Survey"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Get survey"
      },
      "put": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Survey"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "Survey updated"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Update survey"
      }
    },
    "/surveys/{surveyId}/assumptions": {
      "parameters": [
        {
          "in": "path",
          "name": "surveyId",
          "required": true,
          "type": "integer"
        }
      ],
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "List of assumptions in survey",
            "schema": {
              "items": {
                "$ref": "#/definitions/Assumption"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List assumptions in survey"
      }
    },
    "/surveys/{surveyId}/assumptions/{assumptionId}/responses": {
      "parameters": [
        {
          "in": "path",
          "name": "assumptionId",
          "required": true,
          "type": "integer"
        },
        {
          "in": "path",
          "name": "surveyId",
          "required": true,
          "type": "integer"
        }
      ],
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "List of responses for assumption in a survey",
            "schema": {
              "items": {
                "$ref": "#/definitions/Response"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List of responses for assumption in a survey"
      }
    },
    "/surveys/{surveyId}/assumptions/{assumptionId}/responses/latestPerUser": {
      "parameters": [
        {
          "in": "path",
          "name": "surveyId",
          "required": true,
          "type": "integer"
        },
        {
          "in": "path",
          "name": "assumptionId",
          "required": true,
          "type": "integer"
        }
      ],
      "get": {
        "parameters": [
          {
            "description": "Limit results to before datetime",
            "format": "date-time",
            "in": "query",
            "name": "before_datetime",
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Latest response for assumption per user in a survey",
            "schema": {
              "items": {
                "$ref": "#/definitions/Response"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Latest response for assumption per user in a survey"
      }
    },
    "/users": {
      "get": {
        "parameters": [
          {
            "description": "Filter by user UUID",
            "in": "query",
            "name": "uuid",
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "List of users",
            "schema": {
              "items": {
                "$ref": "#/definitions/AppUser"
              },
              "type": "array"
            }
          },
          "404": {
            "$ref": "#/responses/errorNotFound"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List users"
      },
      "post": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AppUser"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "User created",
            "schema": {
              "$ref": "#/definitions/AppUser"
            }
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Create user"
      }
    },
    "/users/{userId}": {
      "parameters": [
        {
          "in": "path",
          "name": "userId",
          "required": true,
          "type": "integer"
        }
      ],
      "delete": {
        "parameters": [],
        "responses": {
          "204": {
            "description": "User deleted"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Delete user"
      },
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "Found user",
            "schema": {
              "$ref": "#/definitions/AppUser"
            }
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Get user"
      },
      "put": {
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AppUser"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "User updated"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Update user"
      }
    },
    "/users/{userId}/surveys/{surveyId}/assumptions/{assumptionId}/responses": {
      "parameters": [
        {
          "in": "path",
          "name": "userId",
          "required": true,
          "type": "integer"
        },
        {
          "in": "path",
          "name": "surveyId",
          "required": true,
          "type": "integer"
        },
        {
          "in": "path",
          "name": "assumptionId",
          "required": true,
          "type": "integer"
        }
      ],
      "get": {
        "parameters": [],
        "responses": {
          "200": {
            "description": "List of responses",
            "schema": {
              "items": {
                "$ref": "#/definitions/Response"
              },
              "type": "array"
            }
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "List responses"
      }
    },
    "/users/{userId}/surveys/{surveyId}/assumptions/{assumptionId}/responses/latest": {
      "parameters": [
        {
          "in": "path",
          "name": "userId",
          "required": true,
          "type": "integer"
        },
        {
          "in": "path",
          "name": "surveyId",
          "required": true,
          "type": "integer"
        },
        {
          "in": "path",
          "name": "assumptionId",
          "required": true,
          "type": "integer"
        }
      ],
      "get": {
        "parameters": [
          {
            "description": "Limit results to before datetime.",
            "format": "date-time",
            "in": "query",
            "name": "before_datetime",
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Latest response",
            "schema": {
              "$ref": "#/definitions/Response"
            }
          },
          "404": {
            "description": "Not found"
          },
          "default": {
            "$ref": "#/responses/errorResponse"
          }
        },
        "summary": "Get latest response"
      }
    }
  },
  "definitions": {
    "AppUser": {
      "description": "Application user",
      "properties": {
        "email": {
          "type": "string",
          "x-nullable": true
        },
        "full_name": {
          "type": "string",
          "x-nullable": true
        },
        "id": {
          "type": "integer"
        },
        "is_admin": {
          "type": "boolean"
        },
        "login": {
          "type": "string",
          "minLength": 1
        },
        "password_hash": {
          "type": "string",
          "format": "byte",
          "x-nullable": true
        },
        "user_uuid": {
          "type": "string"
        }
      },
      "required": [
        "login"
      ],
      "title": "Root Type for AppUser",
      "type": "object"
    },
    "Assumption": {
      "description": "Assumption",
      "properties": {
        "assumption_text": {
          "type": "string",
          "minLength": 1
        },
        "created_datetime": {
          "format": "date-time",
          "type": "string"
        },
        "id": {
          "type": "integer"
        }
      },
      "required": [
        "assumption_text"
      ],
      "title": "Root Type for Assumption",
      "type": "object"
    },
    "Error": {
      "description": "Error response from API",
      "properties": {
        "code": {
          "type": "integer",
          "description": "Error code"
        },
        "message": {
          "description": "Error message",
          "type": "string"
        },
        "cause": {
          "$ref": "#/definitions/Error"
        }
      },
      "required": [
        "code",
        "message"
      ],
      "title": "Root Type for Error",
      "type": "object"
    },
    "Response": {
      "description": "User response for an assumption in a survey.",
      "properties": {
        "app_user_id": {
          "type": "integer",
          "minLength": 1
        },
        "assumption_id": {
          "type": "integer",
          "minLength": 1
        },
        "id": {
          "type": "integer"
        },
        "response_datetime": {
          "format": "date-time",
          "type": "string"
        },
        "survey_id": {
          "type": "integer",
          "minLength": 1
        },
        "value": {
          "type": "boolean"
        }
      },
      "required": [
        "app_user_id",
        "assumption_id",
        "survey_id",
        "value"
      ],
      "title": "Root Type for Response",
      "type": "object"
    },
    "Survey": {
      "description": "Survey",
      "properties": {
        "end_datetime": {
          "format": "date-time",
          "type": "string",
          "x-nullable": true
        },
        "id": {
          "type": "integer"
        },
        "name": {
          "type": "string",
          "minLength": 1
        },
        "start_datetime": {
          "format": "date-time",
          "type": "string"
        }
      },
      "required": [
        "name"
      ],
      "title": "Root Type for Survey",
      "type": "object"
    }
  }
}