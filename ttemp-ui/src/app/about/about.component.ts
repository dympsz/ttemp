import { Component, OnInit } from '@angular/core';

import { environment } from '@/environment';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  version = environment.version;
  authors = [
    { name: 'Dymitr Pszenicyn', email: 'dpszenicyn@gmail.com' },
    { name: 'Andrzej Kacperski', email: 'TODO' },
    { name: 'Tomasz Urbański', email: 'TODO' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
