package handlers

import (
	"time"

	"gitlab.com/dympsz/ttemp/ttemp-srv/log"

	"github.com/volatiletech/null"
	"gitlab.com/dympsz/ttemp/ttemp-srv/datastore"
	"gitlab.com/dympsz/ttemp/ttemp-srv/models"
	dbmodels "gitlab.com/dympsz/ttemp/ttemp-srv/models"

	"github.com/go-openapi/runtime/middleware"
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	apimodels "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
	"gitlab.com/dympsz/ttemp/ttemp-srv/gen/restapi/operations"
)

func InitSurveyHandlers(api *operations.TtempAPI) {
	api.GetSurveysHandler = getSurveysHandler
	api.PostSurveysHandler = postSurveysHandler
	api.DeleteSurveysSurveyIDHandler = deleteSurveysSurveyIDHandler
	api.GetSurveysSurveyIDHandler = getSurveysSurveyIDHandler
	api.PutSurveysSurveyIDHandler = putSurveysSurveyIDHandler

	api.GetSurveysSurveyIDAssumptionsHandler = getSurveysSurveyIDAssumptionsHandler
	api.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler = getSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler
	api.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler = getSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler
}

var getSurveysHandler = operations.GetSurveysHandlerFunc(
	func(params operations.GetSurveysParams, apiUser *apimodels.AppUser) middleware.Responder {

		var surveys models.SurveySlice
		var err error
		if params.UserID == nil {
			surveys, err = datastore.ListSurveys()
		} else {
			userID := swag.Int64Value(params.UserID)
			log.Debug("userId", userID)
			surveys, err = datastore.ListSurveysByUserID(int(userID))
		}
		if err != nil {
			log.Error("getSurveysHandler", err)
			return operations.NewGetSurveysDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(surveys), "surveys")

		payload := DBSurveyListToAPISurveyList(surveys)
		return operations.NewGetSurveysOK().WithPayload(payload)
	})

var postSurveysHandler = operations.PostSurveysHandlerFunc(
	func(params operations.PostSurveysParams, apiUser *apimodels.AppUser) middleware.Responder {
		survey := params.Body
		dbSurvey := APISurveyToDBSurvey(survey)

		_, err := datastore.NewSurvey(dbSurvey)
		if err != nil {
			log.Error("postSurveysHandler", err)
			return operations.NewPostSurveysDefault(500).WithPayload(modelsError(err))
		}

		payload := DBSurveyToAPISurvey(dbSurvey)
		return operations.NewPostSurveysCreated().WithPayload(payload)
	})

var deleteSurveysSurveyIDHandler = operations.DeleteSurveysSurveyIDHandlerFunc(
	func(params operations.DeleteSurveysSurveyIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		surveyID := int(params.SurveyID)

		survey, err := datastore.GetSurvey(surveyID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("deleteSurveysSurveyIDHandler", err)
			return operations.NewDeleteSurveysSurveyIDDefault(500).WithPayload(modelsError(err))
		}

		err = datastore.DeleteSurvey(survey)
		if err != nil {
			log.Error("deleteSurveysSurveyIDHandler", err)
			return operations.NewDeleteSurveysSurveyIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewDeleteSurveysSurveyIDNoContent()
	})

var getSurveysSurveyIDHandler = operations.GetSurveysSurveyIDHandlerFunc(
	func(params operations.GetSurveysSurveyIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		surveyID := int(params.SurveyID)

		survey, err := datastore.GetSurvey(surveyID)
		if err != nil {
			log.Error("getSurveysSurveyIDHandler", err)
			return operations.NewGetSurveysSurveyIDDefault(500).WithPayload(modelsError(err))
		}

		payload := DBSurveyToAPISurvey(survey)
		return operations.NewGetSurveysSurveyIDOK().WithPayload(payload)
	})

var putSurveysSurveyIDHandler = operations.PutSurveysSurveyIDHandlerFunc(
	func(params operations.PutSurveysSurveyIDParams, apiUser *apimodels.AppUser) middleware.Responder {
		surveyID := int(params.SurveyID)
		newSurvey := params.Body

		newDBSurvey := APISurveyToDBSurvey(newSurvey)

		dbSurvey, err := datastore.GetSurvey(surveyID)
		//TODO: detect not found and return different error
		if err != nil {
			log.Error("putSurveysSurveyIDHandler", err)
			return operations.NewPutSurveysSurveyIDDefault(500).WithPayload(modelsError(err))
		}

		if dbSurvey.ID != newDBSurvey.ID {
			return operations.NewPutSurveysSurveyIDDefault(400).WithPayload(
				modelsErrorStringWithCode("Survey ID doesn't match", 400))
		}

		err = datastore.UpdateSurvey(newDBSurvey)
		if err != nil {
			log.Error("putSurveysSurveyIDHandler", err)
			return operations.NewPutSurveysSurveyIDDefault(500).WithPayload(modelsError(err))
		}

		return operations.NewPutSurveysSurveyIDNoContent()
	})

var getSurveysSurveyIDAssumptionsHandler = operations.GetSurveysSurveyIDAssumptionsHandlerFunc(
	func(params operations.GetSurveysSurveyIDAssumptionsParams, apiUser *apimodels.AppUser) middleware.Responder {
		surveyID := int(params.SurveyID)

		assumptions, err := datastore.ListAssumptionsBySurveyID(int(surveyID))
		if err != nil {
			log.Error("getSurveysSurveyIDAssumptionsHandler", err)
			return operations.NewGetSurveysSurveyIDAssumptionsDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(assumptions), "assumptions")

		payload := DBAssumptionListToAPIAssumptionList(assumptions)
		return operations.NewGetSurveysSurveyIDAssumptionsOK().WithPayload(payload)
	})

var getSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler = operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesHandlerFunc(
	func(params operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesParams, apiUser *apimodels.AppUser) middleware.Responder {
		surveyID := int(params.SurveyID)
		assumptionID := int(params.AssumptionID)

		responses, err := datastore.ListResponsesWithFilters(nil, &surveyID, &assumptionID)
		if err != nil {
			log.Error("getSurveysSurveyIDAssumptionsAssumptionIDResponsesHandler", err)
			return operations.NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(responses), "responses")

		payload := DBResponseListToAPIResponseList(responses)
		return operations.NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesOK().WithPayload(payload)
	})

var getSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler = operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandlerFunc(
	func(params operations.GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserParams, apiUser *apimodels.AppUser) middleware.Responder {
		surveyID := int(params.SurveyID)
		assumptionID := int(params.AssumptionID)
		var beforeDateTime *time.Time = nil
		if params.BeforeDatetime != nil {
			tmp := time.Time(*params.BeforeDatetime)
			beforeDateTime = &tmp
		}
		var responses dbmodels.ResponseSlice
		var err error
		if beforeDateTime != nil {
			responses, err = datastore.ListResponsesWithFiltersLatestPerUserBeforeDT(surveyID, assumptionID, *beforeDateTime)
		} else {
			responses, err = datastore.ListResponsesWithFiltersLatestPerUser(surveyID, assumptionID)
		}
		if err != nil {
			log.Error("getSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler", err)
			return operations.NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserDefault(500).WithPayload(modelsError(err))
		}
		log.Debug("There are", len(responses), "responses")

		payload := DBResponseListToAPIResponseList(responses)
		return operations.NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserOK().WithPayload(payload)
	})

// Conversion functions below

func DBSurveyToAPISurvey(survey *dbmodels.Survey) *apimodels.Survey {
	result := apimodels.Survey{}
	result.ID = int64(survey.ID)
	result.Name = swag.String(survey.Name)
	result.StartDatetime = strfmt.DateTime(survey.StartDatetime)
	if survey.EndDatetime.Valid {
		endDateTime := strfmt.DateTime(survey.EndDatetime.Time)
		result.EndDatetime = &endDateTime
	} else {
		result.EndDatetime = nil
	}
	return &result
}

func DBSurveyListToAPISurveyList(surveys []*dbmodels.Survey) []*apimodels.Survey {
	list := []*apimodels.Survey{}
	for _, dbSurvey := range surveys {
		list = append(list, DBSurveyToAPISurvey(dbSurvey))
	}
	return list
}

func APISurveyToDBSurvey(survey *apimodels.Survey) *dbmodels.Survey {
	result := dbmodels.Survey{}
	result.ID = int(survey.ID)
	result.Name = swag.StringValue(survey.Name)
	result.StartDatetime = time.Time(survey.StartDatetime)
	if survey.EndDatetime != nil {
		result.EndDatetime = null.TimeFrom(time.Time(*survey.EndDatetime))
	} else {
		result.EndDatetime = null.TimeFromPtr(nil)
	}
	return &result
}
