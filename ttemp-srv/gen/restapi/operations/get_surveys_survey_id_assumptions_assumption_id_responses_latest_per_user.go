// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"

	models "gitlab.com/dympsz/ttemp/ttemp-srv/gen/models"
)

// GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandlerFunc turns a function with the right signature into a get surveys survey ID assumptions assumption ID responses latest per user handler
type GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandlerFunc func(GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserParams, *models.AppUser) middleware.Responder

// Handle executing the request and returning a response
func (fn GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandlerFunc) Handle(params GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserParams, principal *models.AppUser) middleware.Responder {
	return fn(params, principal)
}

// GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler interface for that can handle valid get surveys survey ID assumptions assumption ID responses latest per user params
type GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler interface {
	Handle(GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserParams, *models.AppUser) middleware.Responder
}

// NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser creates a new http.Handler for the get surveys survey ID assumptions assumption ID responses latest per user operation
func NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser(ctx *middleware.Context, handler GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler) *GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser {
	return &GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser{Context: ctx, Handler: handler}
}

/*GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser swagger:route GET /surveys/{surveyId}/assumptions/{assumptionId}/responses/latestPerUser getSurveysSurveyIdAssumptionsAssumptionIdResponsesLatestPerUser

Latest response for assumption per user in a survey

*/
type GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser struct {
	Context *middleware.Context
	Handler GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserHandler
}

func (o *GetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUser) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewGetSurveysSurveyIDAssumptionsAssumptionIDResponsesLatestPerUserParams()

	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		r = aCtx
	}
	var principal *models.AppUser
	if uprinc != nil {
		principal = uprinc.(*models.AppUser) // this is really a models.AppUser, I promise
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
