package log

import (
	"github.com/sirupsen/logrus"
)

var logger = logrus.New()

func Init() {
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	logger.SetFormatter(customFormatter)
}

func SetLevel(level logrus.Level) {
	logger.SetLevel(level)
}

func Panic(args ...interface{}) {
	logger.Panic(args...)
}

func Fatal(args ...interface{}) {
	logger.Fatal(args...)
}

func Error(args ...interface{}) {
	logger.Error(args...)
}

func Warning(args ...interface{}) {
	logger.Warning(args...)
}

func Warn(args ...interface{}) {
	logger.Warn(args...)
}

func Print(args ...interface{}) {
	logger.Print(args...)
}

func Info(args ...interface{}) {
	logger.Info(args...)
}

func Debug(args ...interface{}) {
	logger.Debug(args...)
}

func Trace(args ...interface{}) {
	logger.Trace(args...)
}

func Printf(format string, args ...interface{}) {
	logger.Printf(format, args...)
}
