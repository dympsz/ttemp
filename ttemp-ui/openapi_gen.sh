#!/bin/bash
docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli generate \
    -i /local/api/Ttemp_swagger2.yaml \
    -g typescript-angular \
    -o /local/out/ts-angular \
    --additional-properties npmName=ttemp-api \
    --additional-properties withInterfaces=true \
    --additional-properties modelPropertyNaming=original
