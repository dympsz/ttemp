import { SharedModule } from './shared/shared.module';
import { AppErrorHandler } from './shared/app-error-handler';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppComponent } from './app.component';
import { QuestionnaireWithResponsesComponent } from './questionnaire-responses/questionnaire-responses.component';
import { AssumptionWithResponseComponent } from './questionnaire-responses/assumption-response/assumption-response.component';
import { ResponseComponent } from './questionnaire-responses/assumption-response/response/response.component';
import { StudyWithAggResponsesComponent } from './study-agg-responses/study-agg-responses.component';
import { AssumptionWithAggResponsesComponent } from './study-agg-responses/assumption-agg-responses/assumption-agg-responses.component';
import { HeaderComponent } from './header/header.component';
import { AssumptionsComponent } from './assumptions/assumptions.component';
import { AboutComponent } from './about/about.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { FooterComponent } from './footer/footer.component';

import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from '@/shared/guards/auth.guard';

import { ApiModule } from 'ttemp-api';
import { BASE_PATH } from 'ttemp-api';
import { environment } from '../environments/environment';

// configuration of API providers
import { Configuration, ConfigurationParameters } from 'ttemp-api';

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    // set configuration parameters here.

    // This is for API testing purpose only (user1):
    apiKeys: { 'X-auth-token' : 'c82506e8-3def-4329-8d96-c461904eb35c'}
  };
  return new Configuration(params);
}

@NgModule({
  declarations: [
    AppComponent,
    QuestionnaireWithResponsesComponent,
    AssumptionWithResponseComponent,
    ResponseComponent,
    StudyWithAggResponsesComponent,
    AssumptionWithAggResponsesComponent,
    HeaderComponent,
    AssumptionsComponent,
    AboutComponent,
    ErrorPageComponent,
    FooterComponent
  ],
  imports: [
    ApiModule.forRoot(apiConfigFactory),
    BrowserModule,
    FormsModule,
    SharedModule,
    AppRoutingModule,
    // make sure to import the HttpClientModule in the AppModule only,
    // see https://github.com/angular/angular/issues/20575
    HttpClientModule
  ],
  providers: [
    AuthGuard,
    { provide: ErrorHandler, useClass: AppErrorHandler },
    { provide: BASE_PATH, useValue: environment.API_BASE_PATH }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
