package e2etests

import (
	"bytes"
	"net/http"
)

const baseURL = "http://localhost:8081/api"

const authTokenAdmin = "95f4cdb4-e10d-4200-aa00-ede942dd3c76"
const authTokenUser1 = "c82506e8-3def-4329-8d96-c461904eb35c"

func sendRequest(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	resp, err := client.Do(req)
	return resp, err
}

func sendGet(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, http.NoBody)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-auth-token", authTokenAdmin)

	resp, err := sendRequest(req)
	return resp, err
}

func sendPost(url string, jsonBody []byte) (*http.Response, error) {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-auth-token", authTokenAdmin)

	resp, err := sendRequest(req)
	if err != nil {
		return nil, err
	}
	// defer resp.Body.Close()

	// fmt.Println("response Status:", resp.Status)
	// fmt.Println("response Headers:", resp.Header)
	// body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println("response Body:", string(body))
	return resp, nil
}
